<?php namespace StudioBosco\MattermostErrorLogger;

use System\Classes\PluginBase;
use VojtaSvoboda\ErrorLogger\Models\Settings;

use Log;
use MattermostHandler\MattermostHandler;

class Plugin extends PluginBase
{

    public $require = ['VojtaSvoboda.ErrorLogger'];

    public function pluginDetails()
    {
        return [
            'name'        => 'studiobosco.mattermosterrorlogger::lang.plugin.name',
            'description' => 'studiobosco.mattermosterrorlogger::lang.plugin.description',
            'author'      => 'Studio Bosco',
            'icon'        => 'icon-bug',
            'homepage'    => 'https://studiobosco.de'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $monolog = Log::getMonolog();
        $this->setMattermostHandler($monolog);

        \Event::listen('backend.form.extendFields', function($widget) {

            if(!$widget->model instanceof \VojtaSvoboda\ErrorLogger\Models\Settings) {
                return;
            }

            $widget->addTabFields([

                'mattermost_enabled' => [
                    'tab'     => 'studiobosco.mattermosterrorlogger::lang.tab.name',
                    'label'   => 'studiobosco.mattermosterrorlogger::lang.fields.mattermost_enabled.label',
                    'type'    => 'switch'
                ],
                'mattermost_webhook_url' => [
                    'tab'      => 'studiobosco.mattermosterrorlogger::lang.tab.name',
                    'label'    => 'studiobosco.mattermosterrorlogger::lang.fields.mattermost_webhook_url.label',
                    'required' => true,
                    'trigger'  => [
                        'action'    => 'show',
                        'field'     => 'mattermost_enabled',
                        'condition' => 'checked'
                    ]
                ],
                'mattermost_level' => [
                    'tab'      => 'studiobosco.mattermosterrorlogger::lang.tab.name',
                    'label'    => 'studiobosco.mattermosterrorlogger::lang.fields.mattermost_level.label',
                    'required' => true,
                    'type'     => 'dropdown',
                    'options'  => \VojtaSvoboda\ErrorLogger\Models\Settings::getErrorLevelOptions(),
                    'trigger'  => [
                        'action'    => 'show',
                        'field'     => 'mattermost_enabled',
                        'condition' => 'checked'
                    ]
                ],
            ]);
        });
    }

    private function setMattermostHandler($monolog)
    {
        $required = ['mattermost_enabled', 'mattermost_webhook_url'];

        if(!$this->checkRequiredFields($required)) {
            return $monolog;
        }

        $webhook_url = Settings::get('mattermost_webhook_url');
        $level = Settings::get('mattermost_level', 100);
        $handler = new MattermostHandler($webhook_url, $level);
        $monolog->pushHandler($handler);

        return $monolog;
    }

    private function checkRequiredFields(array $fields)
    {
        foreach ($fields as $field) {
            $value = Settings::get($field);
            if (!$value || empty($value)) {
                return false;
            }
        }

        return true;
    }
}
