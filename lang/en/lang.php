<?php

    return [

        'plugin' => [
            'name'        => 'Mattermost support for Error Logger',
            'description' => 'Extend Error Logger to support Mattermost',
        ],

        'tab' => [
            'name' => 'Mattermost',
        ],

        'fields' => [
            'mattermost_enabled' => [
                'label' => 'Enable Mattermost logging'
            ],
            'mattermost_webhook_url' => [
                'label' => 'Mattermost Webhook URL'
            ],
            'mattermost_level' => [
                'label'   => 'Logging level',
                'comment' => 'The minimum logging level at which this handler will be triggered'
            ],
        ],


    ];

?>
