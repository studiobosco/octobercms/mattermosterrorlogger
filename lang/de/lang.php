<?php

return [

    'plugin' => [
        'name' => 'Mattermost Error Logger',
        'description' => 'Sendet Fehler-Logs an Mattermost',
    ],

    'tab' => [
        'name' => 'Mattermost',
    ],

    'fields' => [
        'mattermost_enabled' => [
            'label' => 'Mattermost Logging aktivieren',
        ],
        'mattermost_webhook_url' => [
            'label' => 'Mattermost Webhook URL',
        ],
        'mattermost_level' => [
            'label'   => 'Log-Level',
            'comment' => 'Minimales Log level, ab dem an Mattermost gesendet werden soll.',
        ],
    ],
];
